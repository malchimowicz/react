import React from 'react';

const VideoListItem = ({video, onVideoSelect}) => { //zamiast props można użyć składni es6
									//w taki sposób wyciągam dane? ?
// const VideoListItem = (props) => {
	// const video = props.video // dostęp do listy video
	const imageUrl = video.snippet.thumbnails.default.url;

	return (
	<li onClick={() => onVideoSelect(video)} className="list-group-item">
		<div className="video-list media">
			<div className="media-left">
				<img className="media-object" src={imageUrl} />
			</div>

			<div className="media-body">
				<div className="media-heading">
					{video.snippet.title}
				</div>
			</div>
		</div>
	</li>
	)
};
export default VideoListItem;